;;  If pc=0 after executed one instruction, it will be an infinite loop, 
;;  so pc>1 at this case
MOV R0 -1
MOV R1 1
ADD R0 R0 R1
JZ R0 -1
RET R0
;; 1