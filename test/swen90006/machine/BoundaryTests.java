package swen90006.machine;

import java.util.List;
import java.util.ArrayList;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Files;
import java.nio.file.FileSystems;

import org.junit.*;
import static org.junit.Assert.*;

public class BoundaryTests
{
  //Any method annotated with "@Before" will be executed before each test,
  //allowing the tester to set up some shared resources.
  @Before public void setUp()
  {
  }

  //Any method annotated with "@After" will be executed after each test,
  //allowing the tester to release any shared resources used in the setup.
  @After public void tearDown()
  {
  }

  //Any method annotation with "@Test" is executed as a test.
  // @Test public void aTest()
  // {
  //   //the assertEquals method used to check whether two values are
  //   //equal, using the equals method
  //   final int expected = 2;
  //   final int actual = 1 + 1;
  //   assertEquals(expected, actual);
  // }

  // @Test public void anotherTest()
  // {
  //   List<String> list = new ArrayList<String>();
  //   list.add("a");
  //   list.add("b");

  //   //the assertTrue method is used to check whether something holds.
  //   assertTrue(list.contains("a"));
  // }

  // //Test test opens a file and executes the machine
  // @Test public void aFileOpenTest()
  // {
  //   final List<String> lines = readInstructions("examples/array.s");
  //   Machine m = new Machine();
  //   assertEquals(m.execute(lines), 45);
  // }
  
  // //To test an exception, specify the expected exception after the @Test
  // @Test(expected = java.io.IOException.class) 
  //   public void anExceptionTest()
  //   throws Throwable
  // {
  //   throw new java.io.IOException();
  // }

  //This test should fail.
  //To provide additional feedback when a test fails, an error message
  //can be included
  // @Test public void aFailedTest()
  // {
  //   //include a message for better feedback
  //   final int expected = 2;
  //   final int actual = 1 + 2;
  //   assertEquals("Some failure message", expected, actual);
  // }

  @Test(expected = swen90006.machine.NoReturnValueException.class) 
  public void EC_1()
  {
    final List<String> lines = readInstructions("examples/BoundaryTests/EC-1.s");
    Machine m = new Machine();
    m.execute(lines);
  }

  @Test
  public void EC_2()
  {
    final List<String> lines = readInstructions("examples/BoundaryTests/EC-2.s");
    Machine m = new Machine();
    assertEquals(m.execute(lines), 3);
  }

  @Test
  public void EC_3()
  {
    final List<String> lines = readInstructions("examples/BoundaryTests/EC-3.s");
    Machine m = new Machine();
    assertEquals(m.execute(lines), -1);
  }

  @Test (expected = swen90006.machine.InvalidInstructionException.class) 
  public void EC_4()
  {
    final List<String> lines = readInstructions("examples/BoundaryTests/EC-4.s");
    Machine m = new Machine();
    m.execute(lines);
  }

  @Test
  public void EC_5_1()
  {
    final List<String> lines = readInstructions("examples/BoundaryTests/EC-5-1.s");
    Machine m = new Machine();
    assertEquals(m.execute(lines), 4);
  }

  @Test
  public void EC_5_2()
  {
    final List<String> lines = readInstructions("examples/BoundaryTests/EC-5-2.s");
    Machine m = new Machine();
    assertEquals(m.execute(lines), 4);
  }

  @Test (expected = swen90006.machine.InvalidInstructionException.class) 
  public void EC_6()
  {
    final List<String> lines = readInstructions("examples/BoundaryTests/EC-6.s");
    Machine m = new Machine();
    m.execute(lines);
  }

  @Test
  public void EC_7()
  {
    final List<String> lines = readInstructions("examples/BoundaryTests/EC-7.s");
    Machine m = new Machine();
    assertEquals(m.execute(lines), 3);
  }

  @Test (expected = swen90006.machine.InvalidInstructionException.class) 
  public void EC_8()
  {
    final List<String> lines = readInstructions("examples/BoundaryTests/EC-8.s");
    Machine m = new Machine();
    m.execute(lines);
  }

  @Test
  public void EC_9_1()
  {
    final List<String> lines = readInstructions("examples/BoundaryTests/EC-9-1.s");
    Machine m = new Machine();
    assertEquals(m.execute(lines), -65535);
  }

  @Test
  public void EC_9_2()
  {
    final List<String> lines = readInstructions("examples/BoundaryTests/EC-9-2.s");
    Machine m = new Machine();
    assertEquals(m.execute(lines), 65535);
  }

  @Test (expected = swen90006.machine.InvalidInstructionException.class) 
  public void EC_10()
  {
    final List<String> lines = readInstructions("examples/BoundaryTests/EC-10.s");
    Machine m = new Machine();
    m.execute(lines);
  }

  @Test
  public void EC_11()
  {
    final List<String> lines = readInstructions("examples/BoundaryTests/EC-11.s");
    Machine m = new Machine();
    assertEquals(m.execute(lines), 55);
  }

  @Test
  public void EC_12()
  {
    final List<String> lines = readInstructions("examples/BoundaryTests/EC-12.s");
    Machine m = new Machine();
    assertEquals(m.execute(lines), 33);
  }

  @Test
  public void EC_13_1()
  {
    final List<String> lines = readInstructions("examples/BoundaryTests/EC-13-1.s");
    Machine m = new Machine();
    assertEquals(m.execute(lines), 55);
  }

  @Test
  public void EC_13_2()
  {
    final List<String> lines = readInstructions("examples/BoundaryTests/EC-13-2.s");
    Machine m = new Machine();
    assertEquals(m.execute(lines), 55);
  }

  @Test
  public void EC_14()
  {
    final List<String> lines = readInstructions("examples/BoundaryTests/EC-14.s");
    Machine m = new Machine();
    assertEquals(m.execute(lines), 8);
  }

  @Test
  public void EC_15()
  {
    final List<String> lines = readInstructions("examples/BoundaryTests/EC-15.s");
    Machine m = new Machine();
    assertEquals(m.execute(lines), 0);
  }

  @Test(expected = swen90006.machine.NoReturnValueException.class)
  public void EC_16()
  {
    final List<String> lines = readInstructions("examples/BoundaryTests/EC-16.s");
    Machine m = new Machine();
    m.execute(lines);
  }

  @Test
  public void EC_17_1()
  {
    final List<String> lines = readInstructions("examples/BoundaryTests/EC-17-1.s");
    Machine m = new Machine();
    assertEquals(m.execute(lines), 1);
  }

  @Test
  public void EC_17_2()
  {
    final List<String> lines = readInstructions("examples/BoundaryTests/EC-17-2.s");
    Machine m = new Machine();
    assertEquals(m.execute(lines), 0);
  }

  @Test(expected = swen90006.machine.NoReturnValueException.class)
  public void EC_18()
  {
    final List<String> lines = readInstructions("examples/BoundaryTests/EC-18.s");
    Machine m = new Machine();
    m.execute(lines);
  }

  @Test
  public void EC_19()
  {
    final List<String> lines = readInstructions("examples/BoundaryTests/EC-19.s");
    Machine m = new Machine();
    assertEquals(m.execute(lines), 0);
  }

  // @Test(expected = swen90006.machine.NoReturnValueException.class)
  // public void EC_20()
  // {
  //   final List<String> lines = readInstructions("examples/BoundaryTests/EC-20.s");
  //   Machine m = new Machine();
  //   m.execute(lines);
  // }

  @Test(expected = swen90006.machine.NoReturnValueException.class) 
  public void EC_21()
  {
    final List<String> lines = readInstructions("examples/BoundaryTests/EC-21.s");
    Machine m = new Machine();
    m.execute(lines);
  }

  //Read in a file containing a program and convert into a list of
  //string instructions
  private List<String> readInstructions(String file)
  {
    Charset charset = Charset.forName("UTF-8");
    List<String> lines = null;
    try {
      lines = Files.readAllLines(FileSystems.getDefault().getPath(file), charset);
    }
    catch (Exception e){
      System.err.println("Invalid input file! (stacktrace follows)");
      e.printStackTrace(System.err);
      System.exit(1);
    }
    return lines;
  }
}
